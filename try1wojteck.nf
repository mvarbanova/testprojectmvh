nextflow.enable.dsl = 2
params.reference = "./rawdata/*.fasta" 
params.indir = "./rawdata"
//params.reference = "${baseDir}/CARD_v3.0.8_SRST2.fasta"

process fastp {
  publishDir "${params.outdir}/${fastqfile.getSimpleName()}_fastq/", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp:0.22.0--h2e03b76_0"
  input:
    path fastqfile
  output:
    path "fastp_fastq/*.fastq", emit: fastqfile
    path "fastp_report", emit: fastpreport
  script:
    """
    mkdir fastp_fastq
    mkdir fastp_report
    fastp -i ${fastqfile} -o fastp_fastq/${fastqfile.getSimpleName()}_fastp.fastq -h fastp_report/fastp.html -j fastp_report/fastp.json

    """
}

process fastqc {
  publishDir "${params.outdir}/fastqc/${fastqfile.getSimpleName()}/", mode: 'copy', overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input:
    path fastqfile
  output:
    path "fastqc_results", emit: results
  script:
    """
    mkdir fastqc_results
    fastqc ${fastqfile} --outdir fastqc_results
    """
}

process srst2 {
  publishDir "${params.outdir}/srst2/", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/srst2:0.2.0--py27_2"
  input:
    path fastqfile
  output:
    path "*_genes__*__results.txt", emit: textfile
  script:
    """
    srst2 --input_se ${fastqfile[0]} --output test --gene_db ${fastqfile[1]}
    """
}

workflow {
  fastqfile_ch = channel.fromPath("${params.indir}/*.fastq")
  fastp_ch = fastp(fastqfile_ch.flatten())
  fastqc_ch = fastqc(fastp_ch.fastqfile.flatten())
  reference_ch = channel.fromPath(params.reference)
  srst2_ch = srst2(fastp_ch.fastqfile.combine(reference_ch))
}


