// 
nextflow.enable.dsl = 2

params.with_fastqc = false
params.with_stats = false
params.with_fastp = false

process prefetch {
  storeDir "${params.outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra" // output nun eine datei, statt vorher ein pfad
  script:
    """
    prefetch $accession
    """
}

process fastqDump {
    publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
    input:
        path sraresult
    output:
        path "*.fastq"
    script:
    """
    fastq-dump --split-files ${sraresult} > ${sraresult}.splitFilesSRA 
    """
}

process fastp{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true // erschafft nur Ordner
    container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0" 
    input:
        path fastqDump_infile
    output:
        path "${fastqDump_infile.getSimpleName()}_trim.fastq", emit: fastq_files
        path "*fastp.json", emit: json_files
    script:
    """
    fastp -i ${fastqDump_infile} -o ${fastqDump_infile.getSimpleName()}_trim.fastq -j ${fastqDump_infile.getSimpleName()}_trim.fastp.json
    """
}

process fastqc{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true // erschafft nur Ordner
    container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
    input:
        path infile
    output:
        path "fastQC_results/*" //inhalt fuer den oben geschaffenen ordner
    script:
    """
    mkdir fastQC_results
    fastqc -o ./fastQC_results ${infile}
    """
}

// ./fastQC_results damit der ordner nicht ins homeverzeichnis kommt
// o --outdir     Create all output files in the specified output directory.
//                Please note that this directory must exist as the program
//                will not create it.  If this option is not set then the 
//                output file for each sequence file is created in the same
//                directory as the sequence file which was processed.

/*
process quitils{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/ngsutils%3A0.5.9--py27heb79e2c_4"
    input:
        path infile
    output:
        path "quitils_results/*" 
    script:
    """
    mkdir quitils_results
    fastqc -o ./quitils_results ${infile}
    """
}
*/

// Aggregate results from bioinformatics analyses across many samples into a single report:
process multiQC{
    publishDir "${params.outdir}/multiQC", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/multiqc%3A1.9--pyh9f0ad1d_0"
    input:
        path infile
    output:
        path "*"
    script:
    """
    multiqc ${infile}
    """
}


// Ablauf der Prozesse:
workflow {
// SRA Datei download
    sraresult = prefetch(params.accession)
// SRA Datei splitten, fastQ-Dateien
    fastqDump_channel = fastqDump(sraresult)
// fastp zum trimmen:
    fastp_fastq_channel = channel.empty()
    if (params.with_fastp) {
       fastp_channel = fastp(fastqDump_channel.flatten()) // channel 1 x nutzen
       fastp_json_channel = fastp_channel.json_files // output aufteilen
       fastp_fastq_channel = fastp_channel.fastq_files // output aufteilen
   }
// qualitätsauswertung mit getrimmten Daten aus fastp_channel und ohne fastp:
// dafür beide channel concat(), da netxflow jeden Prozess nur einmal verwendet.
// sonst fehler
    all_fastq_channel = fastqDump_channel.flatten().concat(fastp_fastq_channel.flatten())
    // fastqc aufrufen, optional:
    if (params.with_fastqc) { 
        fastqc_channel = fastqc(all_fastq_channel)
    }
/*
    // fastqutils aufrufen, optional:
if (params.with_stats) {
    quitils_channel = quitils(fastqDump_channel.flatten())
    }*/
    
// multiQC:
    multiQC_channel = multiQC(fastqc_channel.collect().concat(fastp_json_channel).collect())
}

//

// nextflow run fastq_fastqutils_einbauen.nf -profile singularity --accession ERR5738971 --outdir ./out/ 
// nextflow run fastq_fastqutils_einbauen.nf -profile singularity --with_fastp --with_fastqc --accession SRR8735664 --outdir ./out/








